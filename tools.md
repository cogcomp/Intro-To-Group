# Tools for writing software and etc.

This is a collection of tools that I found very useful for developing softwares. 

(This is a unordered list)

And I am not trying to argue which editor is the best editor, X is better than Y. The list only reflects my personal choices about the tools I used.

## Learn the tools:

**Learn to use whatever tools you pick**, many tools are design for programmers and are neither easy nor straightforward. However, invest your time and you will benefit a lot from it, otherwise it may even slow you down.


## Maven

No need to introduce, you ***must*** use it(or similar things) to manage dependencies. Never download/import jar files manually.

There is a nice maven tutorial on CCG Wiki. (need link here.)

## Sublime Text 
Sublime text is a very good general purpose editor, with many plugin available. I use it for almost everything other than coding.

Some nice features you shouldn't missed:

    * Multi-cursor support
    * alignment plugin
    * text pastry <- want to insert sequential number into many places?

Use those features together allow you to accomplish many complicated tasks in a few seconds. (for example, adding sequential ID number to 1000 xml nodes.)

[Here is a free video tutorial](http://code.tutsplus.com/courses/perfect-workflow-in-sublime-text-2)

Also, you don't really need to pay for it if you can tolerate the pop-up windows.

## Intellij Idea

Intellij idea is a Integrated development environment, which provides the following features:

* great out-of-box support for maven, sbt, and many other tools. 
* Smart code suggestion and re-formating
* Many complicate re-factoring tools: extract variable/methods, renaming.

It is a $200 software, but it have a free education license, which is great.

## Sequal Pro (only on Mac)
Nice Mysql GUI for Mac.