# Git(and Gitlab) Tutorial for Cogcomp

##### Prerequisite:
This is Git/Gitlab tutorial for Cogcomp, we assumes that you have already knows the basic things about Git and version control.

Please make sure you are comfortable to perform commit/push/pull/merge
If you are not familiar with concepts. There are many interactive git tutorial you can follows, for examples:

* [Code School](https://www.codeschool.com/paths/git): You can try *Try Git* for free but have to paid for the rest of the course. However, I (Hao) highly recommend this Course as the cost is reasonable and the course itself is well-organized.
* [Git - the Simple Guide](http://rogerdudler.github.io/git-guide/) A very simple guide that covers all the basic things.
* [Pro Git](http://git-scm.com/book/en/v2) The most comprehensive tutorial you can found.
* [Git Ready](http://gitready.com/) 
* [Git Best Practices](https://sethrobertson.github.io/GitBestPractices/) A list of rules to follow.

## Git tutorial

Please make sure that you can answer the following questions:

* What is Git
* What is staged changes.
* What is the difference between commit and push
* How to commit, push, and pull.
* How to merge, and handle conflicts
* How to view changes using git log.
* How to create/switch branches.

If you can not answer ***all*** the question above, you should go back to the basic git tutorials listed in **Prerequisite** section.

### Important thing:

There are a couple very import things to remember when using git.

* Do read about it.
* Use `gitignore`. Check out [https://www.gitignore.io/](https://www.gitignore.io/) for how to generate gitignore file.
* Avoid binary files.
* Many other tips in [Git Best Practices](https://sethrobertson.github.io/GitBestPractices/)

### Some tricks:

#### Git alias

Use alias to make your daily job easier by typing less characters. Here are some nice example:

[http://git-scm.com/book/en/v2/Git-Basics-Git-Aliases](http://git-scm.com/book/en/v2/Git-Basics-Git-Aliases)

### Basic Git Operation

#### Tagging/Release

It's recommended to create tags for software releases. this is a known concept, which also exists in SVN. You can create a new tag named 1.0.0 by executing

    git tag 1.0.0 1b2e1d63ff

the 1b2e1d63ff stands for the first 10 characters of the commit id you want to reference with your tag. You can get the commit id by looking at the output of `git log` 


### Commit messages (How to write good commit messages)

#### When to commit

Ideally, you should commit once you finished one single basic features. So you change will be traceable and easy to revert.

#### When to push

WHen you are happy with all your new commits, you will be ready to push. Once you push your commits, they will be available online.

#### Writing good commit message
Many developers have discussion ([example](http://stackoverflow.com/questions/2290016/git-commit-messages-50-72-formatting)). Click the above link to view the discussion.

A good commit message should contains a sentence described what you did in this commit. A good commit message normally contains 50-120 characters.

#### Split commit message 

Use `git rebase --interactive` to edit that earlier commit, run `git reset HEAD~`, and then git add -p to add some, then make a commit, then add some more and make another commit, as many times as you like. When you're done, run git rebase --continue, and you'll have all the split commits earlier in your stack.

Important: Note that you can play around and make all the changes you want, and not have to worry about losing old changes, because you can always run git reflog to find the point in your project that contains the changes you want, (let's call it a8c4ab), and then git reset a8c4ab.


#### Squashing commit message 
Similarly, `git rebase --interactive` can also used to combine multiple commits, `git reset HEAD~10` will reset the head to 10 previous commits.

## Gitlab

### Access Control

Git controls access using SSH-key, view this document for details:

[http://doc.gitlab.com/ce/ssh/README.html](http://doc.gitlab.com/ce/ssh/README.html)

#### SSH keys vs. Deployment Key

SSH key is per-developer based, and gives you read-write access.

Deployment key is per-project based, and gives you read-only access.



### Readme and Other Documentations

#### Markdown 

Markdown is a markup language with plain text formatting syntax designed so that it can be converted to HTML and many other formats using a tool by the same name. Markdown is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor.

For example, this document is also written in Markdown. I personally use **sublime text** + **Marked** to write and view Markdown documents, but there are other options, for example [Stackedit](https://stackedit.io/)

##### Markdown Tutorial

Some Markdown tutorial:

* [http://markdowntutorial.com/](http://markdowntutorial.com/)
* [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)




### Snippets (Gist)
Gist is a simple way to share snippets and pastes with others. All gists are Git repositories, so they are automatically versioned, forkable and usable from Git.

For a list of Git Snippets, you can visit here:
[https://gitlab-beta.engr.illinois.edu/snippets](https://gitlab-beta.engr.illinois.edu/snippets)


### Issues

Issues are a great way to keep track of tasks, enhancements, and bugs for your projects. They are kind of like email—except they can be shared and discussed with the rest of your team. Most software projects have a bug tracker of some kind. GitHub’s tracker is called Issues, and has its own section in every repository.

Additional to this, you can also use it as a TODO lists. So there is no need to create a file called *todo* anymore. And when you done with one issues, just mark them as closed.

Issues are per-project based, so you visit the issue page of your project using this URL:

    https://gitlab-beta.engr.illinois.edu/cogcomp/${project_name}/issues

You will be able to tag Issues with customized tags, and by default it is empty, but you can generate a default set Tags by 

    https://gitlab-beta.engr.illinois.edu/cogcomp/${project_name}/labels

And click `generate` links.


